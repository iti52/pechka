/**
 * Created on 16.05.15.
 */



if ($(window).width() < 768) {
    jQuery(document).ready(function() {
        jQuery('.order-collapse').hide();
    });
    jQuery('.order-collapse-link span').click(function() {
        jQuery(this).parent().parent().parent().find('.order-collapse').toggle(300);
    });
}



jQuery(document).ready(function(){
    jQuery('.faq-item .answer').hide();
    jQuery('.question-form form').hide();
});

jQuery('.question span').click(function() {
    jQuery(this).parent().toggleClass('question-open');
    jQuery(this).parent().parent().find('.answer').toggle(300);
});

jQuery('.question-form .question-btn').click(function(){
    jQuery('.question-form form').show(300);
    jQuery('.question-form .question-btn').toggle(200);
});